## Overview

**futurae-web-sdk** - Provides the Futurae Web Javascript component to be integrated in your web
application.

It handles the communication between the Futurae iframe, your server and the end users.

## Usage

Download the plain JS library from [here](https://gitlab.com/futurae/web/sdk/raw/master/Futurae-Web-SDK-v1.js) or the minified one from [here](https://gitlab.com/futurae/web/sdk/raw/master/Futurae-Web-SDK-v1.min.js). Then you can initialize
it (given that you have also defined an iframe element in your web page):

```javascript
<script src="/path/to/self/hosted/Futurae-Web-SDK-v1.js"></script>
<script>
  Futurae.init({
    'host': 'host',
    'sig_request': sigRequestPassedFromServer,
    'post_action': 'post_action',
    'lang': 'en'
  });
</script>
```

## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).
